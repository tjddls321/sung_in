#!/usr/bin/env python
# coding: utf-8

# In[3]:


import numpy as np
import pandas as pd
from pandas import DataFrame

#datapath
data=pd.read_csv('AppleStore.csv')
data.dropna()
print(data.isnull().any())
data.shape


# In[4]:


li = {}
print(data['user_rating'].values)
for d in data['user_rating'].values:
    if d not in li:
        li[d] = 1
    else:
        li[d] += 1
print(data['user_rating'].isnull().any())
print([type(k) for k in li.keys()])


# In[5]:


data['user_rating']=round(data['user_rating'],1)
data['user_rating']=data['user_rating'].astype('float64')

str(data['user_rating'])
data['user_rating'].value_counts()


# In[6]:


from sklearn import preprocessing

le_class=preprocessing.LabelEncoder()

data['currency']=le_class.fit_transform(data['currency'])

from sklearn import preprocessing

features=['size_bytes','rating_count_tot','rating_count_ver','user_rating']

min_max_scaler=preprocessing.MinMaxScaler()
np_scaled_data=min_max_scaler.fit_transform(data[features])
scaled_data=data.copy()
scaled_data[features]=np_scaled_data

scaled_data.head()
scaled_data['user_rating'].value_counts()


# In[ ]:





# In[16]:


from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error

kf=KFold(n_splits=10,shuffle=True)

features=['size_bytes','rating_count_tot','rating_count_ver']

accrs=[]

fold_idx=1

for train_idx,test_idx in kf.split(scaled_data):
    print('Fold{}'.format(fold_idx))
    train_d,test_d=scaled_data.iloc[train_idx],scaled_data.iloc[test_idx]
    
    train_y=train_d['user_rating']
    train_x=train_d[features]
    
    test_y=test_d['user_rating']
    test_x=test_d[features]
    
    model=LinearRegression()
    model.fit(train_x,train_y)
    print('model {}={}'. format (model,model.score(test_x,test_y)))
    
    predict=model.predict(test_x)
    
    MAE=mean_absolute_error(test_y,predict)
    accrs.append(MAE)
    
    fold_idx+=1
    
    
print(np.average(accrs))


# In[13]:


from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error

kf=KFold(n_splits=10,shuffle=True)

features=['size_bytes','rating_count_tot','rating_count_ver']

accrs=[]

fold_idx=1

for train_idx,test_idx in kf.split(data):
    print('Fold{}'.format(fold_idx))
    train_d,test_d=data.iloc[train_idx],data.iloc[test_idx]
    
    train_y=train_d['user_rating']
    train_x=train_d[features]
    
    test_y=test_d['user_rating']
    test_x=test_d[features]
    
    model=LinearRegression()
    model.fit(train_x,train_y)
    print('model {}={}'. format (model,model.score(test_x,test_y)))
    
    predict=model.predict(test_x)
    
    MAE=mean_absolute_error(test_y,predict)
    accrs.append(MAE)
    
    fold_idx+=1
    
    
print(np.average(accrs))
#label과 features를 정규화했을 때 보다 성능이 낮은 면을 보여주고 있다.
#features를 정규화시키고 label만 정규화를 시키지 않았을 때와 비슷한 성능을 보인다.


# In[14]:





# In[17]:


from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_absolute_error

kf=KFold(n_splits=10,shuffle=True)

features=['size_bytes','rating_count_tot','rating_count_ver','currency']

accrs=[]

fold_idx=1

for train_idx,test_idx in kf.split(scaled_data):
    print('Fold{}'.format(fold_idx))
    train_d,test_d=scaled_data.iloc[train_idx],scaled_data.iloc[test_idx]
    
    train_y=train_d['user_rating']
    train_x=train_d[features]
    
    test_y=test_d['user_rating']
    test_x=test_d[features]
    
    model=LinearRegression()
    model.fit(train_x,train_y)
    print('model {}={}'. format (model,model.score(test_x,test_y)))
    
    predict=model.predict(test_x)
    
    MAE=mean_absolute_error(test_y,predict)
    accrs.append(MAE)
    
    fold_idx+=1
    
    
print(np.average(accrs))

#feature수를 늘릴 수록 MAE가 작아지는 것을 볼 수 있음.


# In[ ]:


#총 정리
#feature수를 늘릴 수록 MAE가 작아지는 것을 볼 수 있음.
#label과 features를 정규화했을 때가 label과 features를 정규화하지 않았을 때 보다 성능이 높은 면을 보여주고 있다.
#features를 정규화시키고 label만 정규화를 시키지 않았을 때가 label과 features를 정규화하지 않았을 때와 비슷하다.
#즉 label의 정규화는 MAE를 감소시키는데 도움을 준다.

